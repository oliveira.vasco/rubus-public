import json
import sys
import time
import serial
import logging
from typing import Optional

from config import PROJECT_DIRECTORY
from collector.utils.models import Device
from mqtt_client.mqtt_client import forward_reply_to_mqtt
from mqtt_client.constants import INNER_CHAMBER_TOPIC_NAME

logging.basicConfig(
    filename=f"{PROJECT_DIRECTORY}/logs/serial_sender.log",
    level=logging.INFO,
    format='%(asctime)s - %(levelname)s - %(message)s'
)
logger = logging.getLogger(__name__)


def generate_serial_connection(device: Device) -> serial.Serial:
    ser = serial.Serial(device.serial_port, device.baudrate)
    logger.info(f"Sleep 5 seconds after opening the port {device.serial_port}")
    time.sleep(5)
    ser.reset_input_buffer()
    ser.reset_output_buffer()
    return ser


def send_message(ser: serial.Serial, message: bytes):
    bytes_written = ser.write(message)
    logger.info(f"Bytes written: {bytes_written}")
    return bytes_written


def consume_serial_reply(ser: serial.Serial, collector_device: str):
    logger.info(f"Sleeping for {seconds} seconds before reply consumption")
    time.sleep(seconds + 1)
    while ser.in_waiting > 0:
        data = json.loads(ser.readline().decode())
        forward_reply_to_mqtt(data, collector_device, INNER_CHAMBER_TOPIC_NAME)


def send_message_to_device(device: Device, instruction: str, seconds: int, value: Optional[int]) -> None:
    if value:
        message_bytes = "{}{}\n".format(instruction[0], value).encode()
    else:
        message_bytes = "{}{}\n".format(instruction[0], seconds).encode()
    ser = generate_serial_connection(device)
    send_message(ser, message_bytes)
    consume_serial_reply(ser, device.collector_device)
    logger.info(f"Message sent to device {device.serial_port}: {message_bytes}")
    ser.close()
    return None

if __name__ == "__main__":
    instruction = sys.argv[1]
    seconds = int(sys.argv[2])
    device_name = sys.argv[3]
    value = int(sys.argv[4]) if len(sys.argv) > 4 else None
    if device_name == 'arduino':
        device = Device("serial", "<device_name>" , "/dev/...", 9600)
    elif device_name == 'esp32':
        device = Device("serial", "<device_name>" , "/dev/...", 115200)
    send_message_to_device(device, instruction, seconds, value)
