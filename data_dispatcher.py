import json
import logging

from config import PROJECT_DIRECTORY
from collector.utils.models import Device
from collector.collect_dict_from_collectors import generate_data
from mqtt_client.mqtt_client import manage_mqtt_client, generate_producer_client_name
from mqtt_client.constants import INNER_CHAMBER_TOPIC_NAME

logging.basicConfig(
    filename=f"{PROJECT_DIRECTORY}/logs/data_dispatcher.log",
    level=logging.INFO,
    format='%(asctime)s - %(levelname)s - %(message)s'
)
logger = logging.getLogger(__name__)


def collect_and_send(data_generator):
    with manage_mqtt_client(generate_producer_client_name()) as client:
        logger.info("Connected to MQTT broker")
        for data in data_generator:
            client.publish(INNER_CHAMBER_TOPIC_NAME, json.dumps(data), 0)
    return

def get_data_from_devices() -> None:
    devices = [
        Device("serial", "<device_name>" , "/dev/...", 115200),
    ]
    data_generator = generate_data(devices, 58)
    collect_and_send(data_generator)
    return None

if __name__ == "__main__":
    get_data_from_devices()
