from contextlib import contextmanager
from datetime import datetime
from random import randint
import json
import logging

from AWSIoTPythonSDK.MQTTLib import AWSIoTMQTTClient

from config import DEVICE_NAME
from mqtt_client.constants import (
    PI_TREE_ROOT_PATH,
    PI_TREE_PRIVATE_KEY_PATH,
    PI_TREE_CERTIFICATE_PATH,
    MQTT_ENDPOINT,
    MQTT_PORT,
)
logger = logging.getLogger(__name__)


def generate_producer_client_name():
    timestamp = datetime.now().strftime("%Y%m%d%H%M%S")
    client_name = f"PRODUCER-{timestamp}-{randint(1000, 9999)}"
    logger.info(f"Client name is {client_name}")
    return client_name


def get_mqtt_client(client_name):
    mqtt_client = AWSIoTMQTTClient(DEVICE_NAME + '-' + client_name)
    mqtt_client.configureEndpoint(MQTT_ENDPOINT, MQTT_PORT)

    mqtt_client.configureCredentials(
        PI_TREE_ROOT_PATH, PI_TREE_PRIVATE_KEY_PATH, PI_TREE_CERTIFICATE_PATH
    )

    mqtt_client.configureOfflinePublishQueueing(-1)
    mqtt_client.configureDrainingFrequency(2)
    mqtt_client.configureConnectDisconnectTimeout(10)
    mqtt_client.configureMQTTOperationTimeout(5)
    
    return mqtt_client

def subscribe(client, topic, callback):
    client.connect()
    client.subscribe(topic, 1, callback)

def publish(client, topic, payload):
    client.connect()
    client.publish(topic, json.dumps(payload), 0)

@contextmanager
def manage_mqtt_client(client_name):
    client = get_mqtt_client(client_name)
    client.connect()
    logger.info("Connected to MQTT broker")
    try:
        yield client
    except Exception as e:
        logger.error(e)
    finally:
        logger.info("Disconnecting from MQTT broker")
        client.disconnect()


def forward_reply_to_mqtt(dict_data: dict, collector_device: str, topic: str):
    with manage_mqtt_client(generate_producer_client_name()) as client:
        logger.info("Connected to MQTT broker")
        dict_data['collector_device'] = collector_device
        if 'created_at' not in dict_data:
            dict_data['created_at'] = datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%S.%f")
        logger.info(dict_data)
        client.publish(topic, json.dumps(dict_data), 0)
