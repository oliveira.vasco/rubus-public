MQTT_ENDPOINT = "<mqtt_endpoint>"
MQTT_PORT = 1111 # 8883 for SSL

PI_TREE_ROOT_PATH = "<root_ca_directory>"
PI_TREE_PRIVATE_KEY_PATH = "<private_key_path>"
PI_TREE_CERTIFICATE_PATH = "<certificate_path>"

TRIGGER_TOPIC_NAME = "<trigger_topic_name>"
INNER_CHAMBER_TOPIC_NAME = "<inner_chamber_topic_name>"
