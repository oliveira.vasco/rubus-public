import time
import logging
from datetime import datetime, timedelta
from collector.utils.serial_collector import SerialCollector
from collector.utils.scd30_stemma_collector import SCD30StemmaCollector
from collector.config import DELAY_BETWEEN_READINGS

logger = logging.getLogger(__name__)


def created_collectors(list_devices):
    collectors = []
    for device in list_devices:
        if device.type == 'serial':
            collectors.append(SerialCollector(device))
        elif device.type == 'scd30':
            collectors.append(SCD30StemmaCollector(device))
    return collectors


def generate_data(list_devices, running_seconds):
    deadline = datetime.now() + timedelta(seconds=running_seconds)
    logger.info(f"Start to collect data")
    collectors = created_collectors(list_devices)
    while datetime.now() < deadline:
        for collector in collectors:
            for _ in range(collector.collection_rounds):
                data = collector.collect()
                logger.info(f"Data: {data}")
                if data:
                    yield data
            collector.clean_channels()
        time.sleep(DELAY_BETWEEN_READINGS)
    [c.close() for c in collectors]
