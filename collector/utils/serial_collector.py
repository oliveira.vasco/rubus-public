import serial
import json
import logging
from datetime import datetime

from .general_collector import GeneralCollector
from .models import Device

logger = logging.getLogger(__name__)


class SerialCollector(GeneralCollector):
    def __init__(self, device: Device, collection_rounds: int = 2):
        self.device = device
        self.ser = serial.Serial(device.serial_port, device.baudrate)
        self.collection_rounds = collection_rounds
    
    def collect(self):
        if self.ser.in_waiting == 0:
            logger.info(f"Serial port {self.device.serial_port} is empty")
            return {}
        try:
            rcv = self.ser.readline()
            logger.info(f"Received data from {self.device.serial_port}: {rcv}")
            if rcv:
                dict_data = json.loads(rcv)
                dict_data['collector_device'] = self.device.collector_device
                dict_data['created_at'] = datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%S.%f")
                return dict_data
        except Exception as e:
            logger.error(f"Failed to parse json {rcv}")
            logger.error(e, exc_info=True)
            return {}
        return {}
    
    def close(self):
        self.ser.close()

    def clean_channels(self):
        self.ser.reset_input_buffer()
        self.ser.reset_output_buffer()
