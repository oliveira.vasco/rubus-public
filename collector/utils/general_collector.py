from abc import ABC

from collector.utils.models import Device


class GeneralCollector(ABC):
    def __init__(self, device: Device, collection_rounds: int = 1):
        pass

    def collect(self):
        pass
    
    def close(self):
        pass
    
    def clean_channels(self):
        pass
