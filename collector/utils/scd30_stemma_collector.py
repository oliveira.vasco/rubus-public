import logging
from datetime import datetime

import adafruit_scd30
from adafruit_extended_bus import ExtendedI2C as I2C

from collector.utils.models import Device
from collector.utils.general_collector import GeneralCollector

logger = logging.getLogger(__name__)


class SCD30StemmaCollector(GeneralCollector):
    def __init__(self, device: Device, collection_rounds: int = 1):
        logger.info("Initializing data collector for SCD30 Stemma")
        self.device = device
        self.i2c_bus = I2C(3)
        self.scd = adafruit_scd30.SCD30(self.i2c_bus)
        self.collection_rounds = collection_rounds

    def collect(self):
        if self.scd.data_available:
            return {
                'event': 'New data',
                'temperature': self.scd.temperature,
                'humidity': self.scd.relative_humidity,
                'co2': self.scd.CO2,
                'millis': 0,
                'sensor': 'SCD30',
                'device': 'RPI',
                'collector_device': self.device.collector_device,
                'created_at': datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%S.%f")
            }
        else:
            return {}
    
    def close(self):
        logger.info("Closing SCD30")
        self.i2c_bus.deinit()
