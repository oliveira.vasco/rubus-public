from typing import Optional
from dataclasses import dataclass


@dataclass
class Device:
    type: str
    collector_device: str
    serial_port: Optional[str] = None
    baudrate: Optional[int] = None
