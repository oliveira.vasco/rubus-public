import os
import logging

logger = logging.getLogger(__name__)

PROJECT_DIRECTORY = "<directory_of_project>"


def write_cron_job(cron_job):
    with open(f"{PROJECT_DIRECTORY}/cron/cron_file", "a") as f:
        f.write(cron_job)

def wipe_cron_job_file():
    with open(f"{PROJECT_DIRECTORY}/cron/cron_file", "w") as f:
        f.write("")

def reset_cron_job_file(client, payload):
    logger.info(f"MQTT consumer: Reset job schedule")
    wipe_cron_job_file()
    for job in payload['jobs']:
        write_cron_job(job)
        write_cron_job("\n")
    result = os.system(f"crontab {PROJECT_DIRECTORY}/cron/cron_file")
    logger.info(f"MQTT consumer: Job reset command result {result}")
