import time
import logging
from config import PROJECT_DIRECTORY
from consumer_child_trigger import process_collection_trigger
from mqtt_client.mqtt_client import get_mqtt_client, subscribe
from mqtt_client.constants import TRIGGER_TOPIC_NAME


logging.basicConfig(
    filename=f"{PROJECT_DIRECTORY}/logs/consumer_info.log",
    level=logging.INFO,
    format='%(asctime)s - %(levelname)s - %(message)s'
)
logger = logging.getLogger(__name__)

def run_consumer():
    logger.info("MQTT consumer: Initiation")
    subscribe(get_mqtt_client("CONSUMER"), TRIGGER_TOPIC_NAME, process_collection_trigger)
    while True:
        logger.info("MQTT consumer: Alive")
        time.sleep(300)
        
if __name__ == '__main__':
    run_consumer()
