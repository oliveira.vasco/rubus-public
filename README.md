## Rubus-public

This project was conceived to bridge the gap between sensors, air pumps, and AWS IoT.
It contains several scripts to manage these three components:
1. `collector` module is responsible for the interface of devices such as a Raspberry pi and ESP32;
2. An `mqtt_client` to report events to AWS;
3. A `data_dispatcher` used for collecting data from sensors and reporting it to AWS;
4. The `event_sender` is used to manage instructions to the air pumps;
5. A MQTT `consumer`used to update cronjobs remotely (currently inactive);