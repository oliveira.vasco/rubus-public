import sys
import logging
from datetime import datetime, timedelta
from typing import Optional

from config import PROJECT_DIRECTORY
from mqtt_client.mqtt_client import forward_reply_to_mqtt
from mqtt_client.constants import INNER_CHAMBER_TOPIC_NAME

logging.basicConfig(
    filename=f"{PROJECT_DIRECTORY}/logs/event_sender.log",
    level=logging.INFO,
    format='%(asctime)s - %(levelname)s - %(message)s'
)
logger = logging.getLogger(__name__)

collector_device = '<device_name>'


def _deduce_event(character: str) -> str:
    if character == 'sas':
        return 'Start air sealed round'
    elif character == 'eas':
        return 'End air sealed round'
    elif character == 'sap':
        return 'Start air pump round'
    elif character == 'eap':
        return 'End air pump round'
    elif character == 'sir':
        return 'Start idle round'
    elif character == 'eir':
        return 'End idle round'
    elif character == 'ssas':
        return 'Scheduled start air sealed round'
    else:
        raise ValueError(f"Unknown event character: {character}")

def send_event_to_aws(event: str, hour_delta: Optional[int] = None) -> None:
    data = {
        'event': event,
        'millis': 0,
        'sensor': 'no sensor',
        'device': 'RPI'
    }
    if hour_delta:
        scheduled_time = datetime.utcnow() + timedelta(hours=hour_delta)
        data['created_at'] = scheduled_time.strftime("%Y-%m-%dT%H:%M:%S.%f")
    forward_reply_to_mqtt(data, collector_device, INNER_CHAMBER_TOPIC_NAME)
    return None


if __name__ == "__main__":
    event_char = sys.argv[1]
    hour_delta = None
    if len(sys.argv) > 2:
        hour_delta = int(sys.argv[2])
    send_event_to_aws(_deduce_event(event_char), hour_delta)
