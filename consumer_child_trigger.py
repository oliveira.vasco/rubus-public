import json
import os
import logging
from cron.cron_job_setter import reset_cron_job_file

logger = logging.getLogger(__name__)

event_mapping = {
    'reset_cronjobs': reset_cron_job_file
}


def process_collection_trigger(client, userdata, message):
    logger.info(f"MQTT consumer: Received Message from AWS IoT Core topic {message.topic}")
    pid = os.fork()
    payload = json.loads(message.payload.decode("utf-8"))
    event = payload['event']
    if event not in event_mapping:
        logger.error(f"MQTT consumer: Unknown event {event}")
        return
    if pid == 0:
        # Child process
        event_mapping[event](client, payload)
    else:
        # Parent process
        pass
    return
